#pragma once

namespace VolundGraphics
{
	struct Position3D
	{
		Position3D();
		Position3D(float _x, float _y, float _z);
		Position3D(const Position3D& position3D);

		float x, y, z;
	};

	struct Colour
	{
		Colour();
		Colour(float _r, float _g, float _b);
		Colour(const Colour& colour);

		float r, g, b;
	};

	struct UV
	{
		UV();
		UV(float _u, float _v);
		UV(const UV& uv);

		float u, v;
	};

	struct Vertex
	{
		Vertex();
		Vertex(const Position3D& position, const Colour& colour, const UV& uv);

		Position3D	position;
		Colour		colour;
		UV			uv;
	};
}