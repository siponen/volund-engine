#pragma once

#include "GraphicsUtil.h"

using VolundGraphics::Position3D;

namespace VolundGraphics
{
	struct QuadSize
	{
		QuadSize();
		QuadSize(const unsigned int _width, const unsigned int _height);

		unsigned int width;
		unsigned int height;
	};

	struct Quad
	{
		Quad();
		Quad(const Position3D& _position, const QuadSize& _quad_size);

		Position3D	position;
		const QuadSize	quad_size;
	};
}