#pragma once

#include <GL\glew.h>

namespace VolundGL
{
	struct VertexBuffer
	{
		VertexBuffer();
		~VertexBuffer();

		void Bind();
		void BufferData();
		void Unbind();

		GLuint vbo;
		GLfloat* vertices;
		GLenum usageMode;
	};
}