#include "GraphicsUtil.h"

VolundGraphics::Position3D::Position3D() : x(0), y(0), z(0) {}

VolundGraphics::Position3D::Position3D(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

VolundGraphics::Position3D::Position3D(const Position3D& position3D) 
	:	x(position3D.x), y(position3D.y), z(position3D.z)
{
}

VolundGraphics::Colour::Colour() : r(255), g(255), b(255) {}

VolundGraphics::Colour::Colour(float _r, float _g, float _b) : r(_r), g(_g), b(_b) {}

VolundGraphics::Colour::Colour(const Colour& colour) : r(colour.r), g(colour.g), b(colour.b) {}


VolundGraphics::UV::UV() : u(0), v(0) {}

VolundGraphics::UV::UV(float _u, float _v) : u(_u), v(_v) {}

VolundGraphics::UV::UV(const UV& uv) : u(uv.u), v(uv.v) {}


VolundGraphics::Vertex::Vertex() : position(Position3D()), colour(Colour()), uv(UV()) {}

VolundGraphics::Vertex::Vertex(
	const Position3D&	_position,
	const Colour&		_colour,
	const UV&			_uv)
	:	position(_position),
		colour(_colour),
		uv(_uv)
{}