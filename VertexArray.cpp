#include "VertexArray.h"

VolundGL::VertexArray::VertexArray()
{
	glGenVertexArrays(1, &vao);
}

VolundGL::VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &vao);
}

void VolundGL::VertexArray::Bind()
{
	glBindVertexArray(vao);
}

void VolundGL::VertexArray::Unbind()
{
	glBindVertexArray(0);
}





