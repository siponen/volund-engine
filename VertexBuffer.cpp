#include "VertexBuffer.h"

VolundGL::VertexBuffer::VertexBuffer()
{
	glGenBuffers(1, &vbo);
}

VolundGL::VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &vbo);
}

void VolundGL::VertexBuffer::Bind()
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

void VolundGL::VertexBuffer::BufferData()
{
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, usageMode);
}

void VolundGL::VertexBuffer::Unbind()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

