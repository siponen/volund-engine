#pragma once

#include <GL\glew.h>
#include <SFML\Graphics.hpp>
#include <SFML\OpenGL.hpp>

#include "VertexBuffer.h"
#include "IndexBuffer.h"

namespace VolundGL
{
	struct VertexArray
	{
		VertexArray();
		~VertexArray();

		void Bind();
		void Unbind();

		void AttachShader();
		void AttachVertexBuffer();
		void AttachIndexBuffer();

		void SetVertexData();

		GLuint vao;
		std::vector<VolundGL::VertexBuffer> vbos;
		std::vector<VolundGL::IndexBuffer> ibos;
		sf::Shader shader;
	};
}