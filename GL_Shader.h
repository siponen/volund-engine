#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>
#include <GL\glew.h>

namespace VolundGL
{
	class Shader
	{
		public:
			Shader();
			~Shader();
			bool LoadFromFile(const std::string &vertex_file, const std::string& fragment_file);
			void SetAttribPointer(const std::string& attrib_name, const GLuint num_vars, const GLuint stride, const GLuint pointer);
			
			GLuint GetProgramID();
		private:
			GLuint LoadShader(const std::string &path, GLenum shader_type );
			
			GLuint m_program_id;
			GLuint m_vert_shader_id;
			GLuint m_frag_shader_id;
	};
}