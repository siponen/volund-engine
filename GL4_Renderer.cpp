#include "GL4_Renderer.h"

VolundGL::GL4_Renderer::GL4_Renderer() : m_resolution_width(800), m_resolution_height(600), m_wireFrameMode(false)
{
	Init();
}

VolundGL::GL4_Renderer::GL4_Renderer(const GLuint _resolutionWidth, const GLuint _resolutionHeight)
	: m_resolution_width(_resolutionWidth), m_resolution_height(_resolutionHeight), m_wireFrameMode(true)
{
	Init();
}

VolundGL::GL4_Renderer::~GL4_Renderer()
{
}

void VolundGL::GL4_Renderer::Init()
{
	//Setup Core shaders
	m_deferred_shader.LoadFromFile("shader/deferred_render.vert", "shader/deferred_render.frag");
	m_deferred_sampler_location = glGetUniformLocation(m_deferred_shader.GetProgramID(), "sampler_fb");

	//Setup Framebuffer vao	
	glGenVertexArrays(1, &m_fbo_vao);
	glBindVertexArray(m_fbo_vao);
	glGenBuffers(1, &m_fbo_vbo);
	glGenBuffers(1, &m_fbo_ebo);
	glBindBuffer(GL_ARRAY_BUFFER, m_fbo_vbo);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), &m_deferred_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_fbo_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint), &m_deferred_indices[0], GL_STATIC_DRAW);

	//Specify the vertex syntax
	// 0 - (x,y) , 1 - (u,v)
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));

	//Setup RenderTexture's framebuffer
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	//Setup texture object
	glGenTextures(1, &m_deferred_texture);
	glBindTexture(GL_TEXTURE_2D, m_deferred_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_resolution_width, m_resolution_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_deferred_texture, 0);

	//Setup Render Buffer object
	glGenRenderbuffers(1, &m_render_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_render_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_resolution_width, m_resolution_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_render_buffer);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	InitScene();
}

virtual void VolundGL::GL4_Renderer::InitScene() {}

void VolundGL::GL4_Renderer::Draw()
{
	//Select the draw mode
	if (m_wireFrameMode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Draw scene to render target
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	glClearColor(0.0f, 0.2f, 0.2f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT );

	DrawScene();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	DrawToScreen();
}

virtual void VolundGL::GL4_Renderer::DrawScene()
{
	drawcall.Draw();
}

void VolundGL::GL4_Renderer::DrawToScreen()
{
	//Draw render target to screen
	glUseProgram(m_deferred_shader.GetProgramID());
	glUniform1i(m_deferred_shader.GetProgramID(), m_deferred_sampler_location); //bind samplers to glTexture units
	glBindVertexArray(m_fbo_vao);

	glActiveTexture(GL_TEXTURE0 + m_deferred_sampler_location);
	glBindTexture(GL_TEXTURE_2D, m_deferred_texture);

	//Switch back to GL_FILL to draw the RTT to screen
	if (m_wireFrameMode)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);

	glBindVertexArray(0);
}

const GLfloat VolundGL::GL4_Renderer::m_deferred_vertices[] =
{
	-1.0f, -1.0f, 0.0f, 1.0f, //v0, lowerLeftCorner
	1.0f, -1.0f, 1.0f, 1.0f, //v1, lowerRightCorner
	-1.0f, 1.0f, 0.0f, 0.0f,  //v2, upperLeftCorner
	1.0f, 1.0f, 1.0f, 0.0f,  //v3, upperRightCorner
};

const GLuint VolundGL::GL4_Renderer::m_deferred_indices[] = { 0,2,1,1,2,3 };