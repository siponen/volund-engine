#include "Drawcall.h"

Volund::Drawcall::Drawcall()
{
	Init();
}

Volund::Drawcall::~Drawcall()
{

}

void Volund::Drawcall::Init()
{
	FillScene();
	FillVertices();

	m_shader.LoadFromFile("shader/main_shader.vert","shader/main_shader.frag");
	m_vp_location = glGetUniformLocation(m_shader.GetProgramID(), "vp");

	//Setup OpenGL buffers
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ebo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Vertex), &m_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), &m_indices[0], GL_STATIC_DRAW);

	//Specify the Vertex layout
	//0 - (x,y,z), 1 - (r,g,b), 2 - (u,v)
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(6 * sizeof(GLfloat)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Transformation matrices
	m_projection = glm::perspective(
		glm::radians(45.0f),	//FoV
		(16.0f / 9.0f),			// Aspect Ratio
		0.1f,					// Near clipping plane
		100.0f);				//Far clipping plane

	//configure camera
	glm::vec3 position(15, 10, -10);
	m_view = glm::lookAt(position, position + glm::vec3(0,0,1), glm::vec3(0, 1, 0));
	m_projection = glm::perspective(90.0f, (16.0f / 9.0f), 0.1f, 100.0f);
	m_vp = m_projection * m_view;
}

void Volund::Drawcall::FillScene()
{
	m_quads.push_back(Quad(Position3D(0, 0, 0), QuadSize(1, 1)));
}

void Volund::Drawcall::FillVertices()
{
	unsigned int numVerticesOnWidth(100);
	unsigned int numVerticesOnLength(100);

	unsigned int numCellsOnWidth = numVerticesOnWidth - 1;
	unsigned int numCellsOnLength = numVerticesOnLength - 1;
	unsigned int numCells = numCellsOnWidth*numCellsOnLength;

	unsigned int row;
	unsigned int startIndex;

	unsigned int upperLeftCorner;
	unsigned int upperRightCorner;
	unsigned int lowerLeftCorner;
	unsigned int lowerRightCorner;

	VolundGraphics::Vertex vertex;
	for (GLfloat z = 0; z < numVerticesOnLength; ++z)
	{
		for (GLfloat x = 0; x < numVerticesOnWidth; ++x)
		{
			vertex.position.x = x;
			vertex.position.y = z;
			vertex.position.z = 0;

			vertex.colour.r = float(x*0.04);
			vertex.colour.g = float(1 - z*0.04);
			vertex.colour.b = float(z*0.04);

			m_vertices.push_back(vertex);
		}
	}

	for (unsigned int z = 0; z < numCellsOnLength; ++z)
	{
		row = z*numVerticesOnWidth;
		for (unsigned int x = 0; x < numCellsOnWidth; ++x)
		{
			startIndex = row + x;
			upperLeftCorner = startIndex + numVerticesOnWidth;
			upperRightCorner = upperLeftCorner + 1;
			lowerLeftCorner = startIndex;
			lowerRightCorner = startIndex + 1;
			// |/    
			m_indices.push_back(upperLeftCorner);
			m_indices.push_back(upperRightCorner);
			m_indices.push_back(lowerLeftCorner);
			// /|
			m_indices.push_back(lowerLeftCorner);
			m_indices.push_back(upperRightCorner);
			m_indices.push_back(lowerRightCorner);
		}
	}

	// 0 1
	// |\|
	// 2 3
	/*
	m_vertices.push_back(Vertex(Position3D(-0.5,  1, 0), Colour(1, 0, 0), UV(0, 0)));
	m_vertices.push_back(Vertex(Position3D( 0.5,  1, 0), Colour(0, 1, 0), UV(0, 0)));
	m_vertices.push_back(Vertex(Position3D(-0.5, -1, 0), Colour(0, 0, 1), UV(0, 0)));
	m_vertices.push_back(Vertex(Position3D( 0.5, -1, 0), Colour(1, 1, 1), UV(0, 0)));

	//0,3,2 || 0,1,3
	m_indices.push_back(0);
	m_indices.push_back(3);
	m_indices.push_back(2);

	m_indices.push_back(0);
	m_indices.push_back(1);
	m_indices.push_back(3);
	*/
}

void Volund::Drawcall::Draw()
{
	glBindVertexArray(m_vao);
	glUseProgram(m_shader.GetProgramID());
	glUniformMatrix4fv(m_vp_location, 1, false, glm::value_ptr(m_vp));
	
	glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, (void*)0);

	glBindVertexArray(0);
}