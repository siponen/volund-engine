#pragma once

#include <GL\glew.h>

//Volund Engine
#include "GL_Shader.h"
#include "Quad.h"
#include "GraphicsUtil.h"

//Volund
#include "Drawcall.h"

using VolundGraphics::Vertex;
using VolundGraphics::Position3D;
using VolundGraphics::Colour;
using VolundGraphics::UV;
using VolundGraphics::Quad;
using VolundGraphics::QuadSize;

namespace VolundGL
{
	class GL4_Renderer
	{
	public:
		GL4_Renderer();
		GL4_Renderer(const GLuint _resolutionWidth, const GLuint _resolutionHeight);
		~GL4_Renderer();

		void Draw();

		virtual void InitScene();
		virtual void DrawScene();
	protected:
		//Deferred Rendering
		GLboolean m_wireFrameMode;
		GLuint m_resolution_width;
		GLuint m_resolution_height;

		VolundGL::Shader m_deferred_shader;
		VolundGL::Shader m_geometry_shader;

		GLuint m_deferred_sampler_location;

		GLuint m_fbo;
		GLuint m_fbo_vao, m_fbo_vbo, m_fbo_ebo;

		GLuint m_deferred_texture;
		GLuint m_render_buffer;

		const static GLfloat m_deferred_vertices[];
		const static GLuint	m_deferred_indices[];

		Volund::Drawcall drawcall;
	private:
		void Init();
		void DrawToScreen();
	};
}