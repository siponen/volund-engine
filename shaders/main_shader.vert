#version 330

layout(location=0) in vec3 model;
layout(location=1) in vec3 color;
layout(location=2) in vec2 uv;

uniform mat4 vp;

out vec3 frag_color;

void main()
{
   frag_color = color;
   gl_Position = vp * vec4(model, 1.0);
}