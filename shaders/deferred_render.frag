#version 330
in vec2 uv_frag;

out vec4 out_color;

uniform sampler2D sampler_fb;

void main()
{
	out_color = texture(sampler_fb, uv_frag);
}