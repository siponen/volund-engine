#include "Quad.h"

VolundGraphics::QuadSize::QuadSize() : width(1), height(1) {}
VolundGraphics::QuadSize::QuadSize(const unsigned int _width,
	const unsigned int _height)
	: width(_width),
	height(_height)
{}

VolundGraphics::Quad::Quad(const Position3D& _position, const QuadSize& _quad_size)
	: position(_position),
	quad_size(_quad_size)
{}