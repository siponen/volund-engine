#pragma once

#include "GL\glew.h"

namespace VolundGL
{
	struct IndexBuffer
	{
		IndexBuffer();
		~IndexBuffer();

		void Bind();
		void BufferData();
		void Unbind();

		GLuint ibo;
		GLuint* indices;
		GLenum usageMode;
	};
}
