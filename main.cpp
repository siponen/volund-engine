#include <GL\glew.h>
#include <SFML\Graphics.hpp>
#include <SFML\OpenGL.hpp>

#include "VertexBuffer.h"
#include "GL4_Renderer.h"

int main()
{
	unsigned int appWidth = 1920;
	unsigned int appHeight = 1080;

	sf::RenderWindow window( sf::VideoMode(appWidth, appHeight), "VolundEngine", sf::Style::None );

	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	VolundGL::GL4_Renderer renderer(appWidth, appHeight);
	renderer.InitScene();

	while ( window.isOpen() )
	{
		//Event handling
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();

			if (event.type == sf::Event::Closed)
				window.close();
		}

		renderer.Draw();

		//Draw
		window.display();
	}

	return 0;
}