#pragma once
//STD
#include <vector>
//GLEW
#include <GL\glew.h>
//GLM
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <glm\gtc\type_ptr.hpp>

//Volund Engine
#include "GraphicsUtil.h"
#include "Quad.h"
#include "GL_Shader.h"

using VolundGraphics::Vertex;
using VolundGraphics::Position3D;
using VolundGraphics::Colour;
using VolundGraphics::UV;
using VolundGraphics::Quad;
using VolundGraphics::QuadSize;

namespace Volund
{
	class Drawcall
	{
	public:
		Drawcall();
		~Drawcall();

		void Draw();

	private:
		void Init();
		void FillScene();
		void FillVertices();

		//OpenGL Fluff
		VolundGL::Shader m_shader;
		GLuint m_vp_location;
		GLuint m_vao;
		GLuint m_vbo;
		GLuint m_ebo;

		//Transformation matrices
		glm::mat4 m_view;
		glm::mat4 m_projection;
		glm::mat4 m_vp;

		//Geometry
		std::vector<Quad>	m_quads;
		std::vector<Vertex>	m_vertices;
		std::vector<GLuint> m_indices;
	};
}