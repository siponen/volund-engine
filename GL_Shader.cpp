#include "GL_Shader.h"

VolundGL::Shader::Shader()
{
}

VolundGL::Shader::~Shader()
{
}

bool VolundGL::Shader::LoadFromFile(const std::string &vertexPath,
	const std::string& fragmentPath)
{	
	//Load & attach vertex shader
	m_vert_shader_id = LoadShader(vertexPath, GL_VERTEX_SHADER);
	m_program_id = glCreateProgram();
	if( m_vert_shader_id == 0 )
	{
		glDeleteProgram( m_program_id);
		m_program_id = 0;
		std::cout << "No valid Vertex shader, deleting program" << std::endl;
		return false;
	}
	glAttachShader( m_program_id, m_vert_shader_id );
	
	//Load & attach fragment shader
	m_frag_shader_id = LoadShader(fragmentPath, GL_FRAGMENT_SHADER);
	if(m_frag_shader_id == 0)
	{
		glDeleteShader(m_vert_shader_id);
		glDeleteProgram( m_program_id );
		m_program_id = 0;
		std::cout << "No valid fragment shader, deleting program" << std::endl;
		return false;
	}
	glAttachShader( m_program_id, m_frag_shader_id );
	glBindFragDataLocation(m_program_id, 0, "out_color");
	glLinkProgram( m_program_id );

	//Check for errors
    GLint program_success = GL_TRUE;
    glGetProgramiv( m_program_id, GL_LINK_STATUS, &program_success );
    if( program_success != GL_TRUE )
    {
        printf( "Error linking program %d!\n", m_program_id );
        glDeleteShader( m_vert_shader_id );
        glDeleteShader( m_frag_shader_id );
        glDeleteProgram( m_program_id );
        m_program_id = 0;
        return false;
    }

	//Clean up excess shader references
    glDeleteShader( m_vert_shader_id );
    glDeleteShader( m_frag_shader_id );

	return true;
}

void VolundGL::Shader::SetAttribPointer(const std::string& attrib_name, const GLuint num_vars, const GLuint total_vars, const GLuint pointer_offset)
{
	GLint attrib = glGetAttribLocation(m_program_id, attrib_name.c_str());
	glEnableVertexAttribArray(attrib);
	glVertexAttribPointer(attrib, num_vars, GL_FLOAT, GL_FALSE, total_vars*sizeof(GLfloat), (void*)(pointer_offset * sizeof(GLfloat)) );
}

GLuint VolundGL::Shader::GetProgramID() { return m_program_id; }

GLuint VolundGL::Shader::LoadShader(const std::string &path, GLenum shader_type )
{
	//Load file
	GLuint shader_id( glCreateShader(shader_type) );
	std::streampos size;
	std::string content;
	const GLchar* content_ptr;
	//char* content;
	std::ifstream shader_file(path);
	if( shader_file.is_open() )
	{
		content.assign(std::istreambuf_iterator< char >(shader_file),
			std::istreambuf_iterator< char >() );
		content_ptr = content.c_str();

		shader_file.close();

		glShaderSource(shader_id,1,(const GLchar**)& content, NULL);
		glCompileShader(shader_id);
	}
	else
	{
		std::cout << "Error, couldn't open shader file: " << path  << std::endl;
		glDeleteShader( shader_id );
		shader_id = 0;
	}

	//Check for errors
	GLint shader_compiled = GL_FALSE;
	glGetShaderiv( shader_id, GL_COMPILE_STATUS, &shader_compiled );
    if( shader_compiled != GL_TRUE )
    {
		printf( "Unable to compile shader %d!\n\nSource:\n%s\n", shader_id, content.c_str() );
		glDeleteShader( shader_id );
		shader_id = 0;
	}

	return shader_id;
}
