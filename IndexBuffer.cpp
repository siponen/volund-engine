#include "IndexBuffer.h"

VolundGL::IndexBuffer::IndexBuffer()
{
	glGenBuffers(1, &ibo);
}

VolundGL::IndexBuffer::~IndexBuffer()
{
	glDeleteBuffers(1, &ibo);
}

void VolundGL::IndexBuffer::Bind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
}

void VolundGL::IndexBuffer::BufferData()
{
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, usageMode);
}

void VolundGL::IndexBuffer::Unbind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}